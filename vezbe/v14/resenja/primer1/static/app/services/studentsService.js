export default {
    findAll() {
        return axios.get("api/studenti");
    },
    findOne(id) {
        return axios.get(`api/studenti/${id}`);
    },
    create(student) {
        return axios.post("api/studenti", student);
    },
    update(id, student) {
        return axios.put(`api/studenti/${id}`, student);
    },
    delete(id) {
        return  axios.delete(`api/studenti/${id}`);
    }
}