import studentsService from '../services/studentsService.js';

export default {
    props: ["studenti"],
    emits: ["ukloniStudenta"],
    data() {
    },
    created() {
    },
    methods: {
        ukloni(indeks) {
            this.$emit("ukloniStudenta", indeks);
        },

    },
    template: `
    <table>
    <thead>
        <tr>
            <th>Broj indeksa</th>
            <th>Ime</th>
            <th>Prezime</th>
            <th>Prosečna ocena</th>
            <th>Smer</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        <tr v-for="(s, indeks) in studenti">
            <td>{{s.brojIndeksa}}</td>
            <td>{{s['ime']}}</td>
            <td>{{s.prezime}}</td>
            <td>{{s.prosecnaOcena}}</td>
            <td>{{s.naziv}} [{{s.sifraSmera}}]</td>
            <td>
                <button @click="ukloni(s.brojIndeksa)">Ukloni</button>
                <router-link :to="'/forma/' + s.brojIndeksa">Forma sa argumentom</router-link>
                <!-- <button @click="odaberiStudenta(s)">Izmeni</button> -->
            </td>
        </tr>
    </tbody>
    <tfoot>
    </tfoot>
</table>
    `
}