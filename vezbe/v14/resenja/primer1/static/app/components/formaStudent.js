import studentsService from "../services/studentsService.js";

export default {
    emits: ["kreiranjeStudenta"],
    created() {
        axios.get("api/smerovi").then(response => {
            this.smerovi = response.data;
        });

        studentsService.findOne(`${this.$route.params.godina}/${this.$route.params.brojIndeksa}`).then(x=>{
            this.noviStudent = x.data;
            this.noviStudent.smer = {
                naziv: this.noviStudent.naziv,
                id: this.noviStudent.smer_id,
                sifraSmera: this.noviStudent.sifraSmera
            }
        })
    },
    
    data() {
        return {
            noviStudent: {},
            smerovi: []
        }
    },
    methods: {
        dodajStudenta() {
            let s = {...this.noviStudent};
            s.smer = s['smer']['id'];

            if(this.$route.params.godina != undefined && this.$route.params.brojIndeksa != undefined) {
                studentsService.update(`${this.$route.params.godina}/${this.$route.params.brojIndeksa}`, s).then(x=>{
                    this.$router.push("/");
                });
            } else {
                studentsService.create(s).then(x=>{
                    this.$router.push("/");
                });
            }

        }
    },
    template: `
    <p>{{$route.params.godina}}/{{$route.params.brojIndeksa}}</p>
    <form v-on:submit.prevent="dodajStudenta">
        <div>
            <label for="brojIndeksa">Broj indeksa: </label>
            <input v-model="noviStudent.brojIndeksa" name="brojIndeksa" required>
        </div>
        <div>
            <label>Smer: <select v-model="noviStudent.smer" name="smer">
                    <option v-for="smer in smerovi" :value="smer">{{smer.naziv}}</option>
                </select>
            </label>
        </div>
        <div>
            <label>Ime: <input v-model="noviStudent.ime" name="ime" required></label>
        </div>
        <div>
            <label>Prezime: <input v-model="noviStudent.prezime" name="prezime" required></label>
        </div>
        <div>
            <label>Prosečna ocena: <input v-model="noviStudent.prosecnaOcena" type="number" name="prosecnaOcena"
                    min="5" max="10" required></label>
        </div>
        <div>
            <button type="submit">Dodaj</button>
        </div>
    </form>
    `
}