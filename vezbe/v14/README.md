# Vežbe 14
*Cilj vežbi: Implementacija servisnog sloja.*

## Zadaci
1. Prepraviti zadatak sa prethodnoh vežbi tako da se funkcije za pristup i obradu podataka izdvoje u posebni servisni sloj. Za svaki entitet obezbediti po jedan, poseban, servis.
___
### Dodatne napomene:
* Dokumentacija za Vue.js radni okvir: https://vuejs.org/guide/introduction.html
* Dokumentacija za Vue router: https://router.vuejs.org/
___
