import flask
from flask import Flask
from flaskext.mysql import MySQL
import pymysql

import json

app = Flask(__name__, static_url_path="/", static_folder="static")
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "studenti"


@app.route("/api/studenti", methods=["GET"])
def dobavi_sve_studente():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studenti.studenti LEFT JOIN smerovi ON studenti.smer_id = smerovi.id")
    studenti = cursor.fetchall()
    return flask.jsonify(studenti)

@app.route("/api/studenti/<string:godina_upisa>/<string:broj_indeksa>", methods=["GET"])
# @app.route("/api/studenti/<path:broj_indeksa>")
def dobavi_jednog_studenta(godina_upisa, broj_indeksa):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studenti.studenti LEFT JOIN smerovi ON studenti.smer_id = smerovi.id WHERE brojIndeksa = %s", (godina_upisa + "/" + broj_indeksa,))
    student = cursor.fetchone()

    if student is None:
        return flask.jsonify(None), 404
    
    return flask.jsonify(student)

@app.route("/api/studenti", methods=["POST"])
def dodaj_studenta():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO studenti (brojIndeksa, ime, prezime, prosecnaOcena, smer_id) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(prosecnaOcena)s, %(smer)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@app.route("/api/studenti/<string:godina_upisa>/<string:broj_indeksa>", methods=["PUT"])
def izmeni_studenta(godina_upisa, broj_indeksa):
    db = mysql.get_db()
    cursor = db.cursor()

    data = dict(flask.request.json)
    data["studentZaIzmenu"] = godina_upisa + "/" + broj_indeksa

    modified = cursor.execute("UPDATE studenti SET brojIndeksa=%(brojIndeksa)s, ime=%(ime)s, prezime=%(prezime)s, prosecnaOcena=%(prosecnaOcena)s, smer_id=%(smer)s WHERE brojIndeksa=%(studentZaIzmenu)s", data)
    db.commit()

    if modified == 0:
        return flask.jsonify(None), 404

    return flask.jsonify(flask.request.json), 200


@app.route("/api/studenti/<string:godina_upisa>/<string:broj_indeksa>", methods=["DELETE"])
def ukloni_jednog_studenta(godina_upisa, broj_indeksa):
    db = mysql.get_db()
    cursor = db.cursor()
    modified = cursor.execute("DELETE FROM studenti.studenti WHERE brojIndeksa = %s", (godina_upisa + "/" + broj_indeksa,))
    db.commit()
    
    if modified == 0:
        return flask.jsonify(None), 404
    
    return flask.jsonify(None), 200

@app.route("/")
def home():
    return app.send_static_file("index.html")

if __name__  == "__main__":
    app.run()