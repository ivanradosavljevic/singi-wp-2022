import json
import base64
import hmac

def generate_token(secret, payload):
    header = base64.urlsafe_b64encode(json.dumps({
        "alg": "HS256",
        "typ": "JWT"
    }).encode()).decode().replace("=", "")
    tmp_pad = base64.urlsafe_b64encode(json.dumps(payload).encode()).decode()
    print(len(tmp_pad))
    print(len(tmp_pad)/4)

    payload = base64.urlsafe_b64encode(json.dumps(payload).encode()).decode().replace("=", "")
    message = f"{header}.{payload}"
    signature = base64.urlsafe_b64encode(hmac.new(secret.encode(), message.encode(), "sha256").digest()).decode().replace("=", "")
    
    return f"{message}.{signature}"

def validate_token(secret, token):
    if token is None:
        return False
    values = token.rsplit(".", 1)
    if len(values) != 2:
        return False
    message, original_signature = values
    signature = base64.urlsafe_b64encode(hmac.new(secret.encode(), message.encode(), "sha256").digest()).decode().replace("=", "")
    return signature == original_signature

def get_claims(secret, token):
    if validate_token(secret, token) == False:
        return None
    
    return json.loads(base64.urlsafe_b64decode((token.split(".")[1]+"==").encode()).decode())
