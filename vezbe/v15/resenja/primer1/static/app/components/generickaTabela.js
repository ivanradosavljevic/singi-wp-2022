import studentsService from '../services/studentsService.js';

export default {
    props: ["redovi", "metadata"],
    emits: ["action"],
    data() {
    },
    created() {
    },
    methods: {
        onAction(action, row) {
            this.$emit("action", {action, row});
        },
    },
    template: `
    <table>
    <thead>
        <tr>
            <th v-for="c in metadata.columns">
                {{c['title']}}
            </th>
            <th>
                Akcije
            </th>
        </tr>
    </thead>

    <tbody>
        <tr v-for="(r, indeks) in redovi">
            <td v-for="c in metadata.columns">
                {{c['transform']?  c['transform'](r[c['key']], r) : r[c['key']]}}
            </td>
            <td>
                <span v-for="a in metadata.actions">
                    <button @click="onAction(a.name, r)">{{a.title}}</button>
                </span>
            </td>
        </tr>
    </tbody>
    <tfoot>
    </tfoot>
</table>
    `
}