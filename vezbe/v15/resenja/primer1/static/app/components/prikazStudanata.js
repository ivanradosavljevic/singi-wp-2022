import studentsService from '../services/studentsService.js';

export default {
    props: [],
    emits: ["ukloniStudenta"],
    data() {
        return {
            studenti: [],
            metadata: {
                columns: [
                    {"key": "brojIndeksa", "title": "Broj indeksa", transform: (cell)=>cell.replace("/", "")},
                    {"key": "ime", "title": "Ime"},
                    {"key": "prezime", "title": "Prezime"},
                    {"key": "prosecnaOcena", "title": "Prosečna ocena"},
                    {"key": "smer", "title": "Smer", transform: (cell, row)=>`${row.naziv} [${row.sifraSmera}]`},
                ],
                actions: [
                    {name: "ukloni", "title": "Ukloni"},
                    {name: "izmeni", "title": "Izmeni"}
                ]
            },
        }
    },
    created() {
        studentsService.findAll().then(x => {
            this.studenti = x.data;
        });
    },
    methods: {
        ukloni(indeks) {
            studentsService.delete(indeks).then(x => {
                studentsService.findAll().then(x => {
                    this.studenti = x.data;
                })
            })
        },
        onAction(event) {
            if(event.action == "izmeni") {
                this.$router.push(`/forma/${event.row.brojIndeksa}`);
            } else if(event.action == "ukloni") {
                this.ukloni(event.row.brojIndeksa);
            }
        }

    },
    template: `
    <tabela-studenata @ukloniStudenta="ukloni" :studenti="studenti"></tabela-studenata>
    <genericka-tabela @action="onAction($event)" :redovi="studenti" :metadata="metadata"></genericka-tabela>
    `
}