import flask
from flask import Flask
from utils.db import mysql
from blueprints.studenti_blueprint import studenti_blueprint
from blueprints.smerovi_blueprint import smerovi_blueprint
from blueprints.login_blueprint import login_blueprint

app = Flask(__name__, static_url_path="/", static_folder="static")

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "studenti"

app.config["SECRET"] = "Neki tekst za secret"

mysql.init_app(app)

app.register_blueprint(studenti_blueprint, url_prefix="/api/studenti")
app.register_blueprint(smerovi_blueprint, url_prefix="/api/smerovi")
app.register_blueprint(login_blueprint, url_prefix="/api/login")

@app.route("/")
def home():
    return app.send_static_file("index.html")

if __name__  == "__main__":
    app.run()