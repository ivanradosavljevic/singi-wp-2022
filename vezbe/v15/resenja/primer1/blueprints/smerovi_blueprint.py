import flask
from flask.blueprints import Blueprint

from utils.db import mysql

smerovi_blueprint = Blueprint("smerovi", __name__)

@smerovi_blueprint.route("/", methods=["GET"])
def dobavi_sve_smerove():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studenti.smerovi")
    smerovi = cursor.fetchall()
    return flask.jsonify(smerovi)