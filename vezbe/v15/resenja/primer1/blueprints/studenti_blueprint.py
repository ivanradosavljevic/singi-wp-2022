import flask
from flask.blueprints import Blueprint

from utils.db import mysql
from utils.jwt import get_claims

studenti_blueprint = Blueprint("students", __name__)

@studenti_blueprint.route("/", methods=["GET"])
def dobavi_sve_studente():
    claims = get_claims(flask.current_app.config["SECRET"], flask.request.headers.get("Authorization"))
    if (claims is None) or ("ADMIN" not in claims["uloge"]):
        return flask.jsonify(""), 403
    
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studenti.studenti LEFT JOIN smerovi ON studenti.smer_id = smerovi.id")
    studenti = cursor.fetchall()
    return flask.jsonify(studenti)

@studenti_blueprint.route("/<string:godina_upisa>/<string:broj_indeksa>", methods=["GET"])
def dobavi_jednog_studenta(godina_upisa, broj_indeksa):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM studenti.studenti LEFT JOIN smerovi ON studenti.smer_id = smerovi.id WHERE brojIndeksa = %s", (godina_upisa + "/" + broj_indeksa,))
    student = cursor.fetchone()

    if student is None:
        return flask.jsonify(None), 404
    
    return flask.jsonify(student)

@studenti_blueprint.route("/", methods=["POST"])
def dodaj_studenta():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO studenti (brojIndeksa, ime, prezime, prosecnaOcena, smer_id) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(prosecnaOcena)s, %(smer)s)", flask.request.json)
    db.commit()
    return flask.jsonify(flask.request.json), 201

@studenti_blueprint.route("/<string:godina_upisa>/<string:broj_indeksa>", methods=["PUT"])
def izmeni_studenta(godina_upisa, broj_indeksa):
    db = mysql.get_db()
    cursor = db.cursor()

    data = dict(flask.request.json)
    data["studentZaIzmenu"] = godina_upisa + "/" + broj_indeksa

    modified = cursor.execute("UPDATE studenti SET brojIndeksa=%(brojIndeksa)s, ime=%(ime)s, prezime=%(prezime)s, prosecnaOcena=%(prosecnaOcena)s, smer_id=%(smer)s WHERE brojIndeksa=%(studentZaIzmenu)s", data)
    db.commit()

    if modified == 0:
        return flask.jsonify(None), 404

    return flask.jsonify(flask.request.json), 200


@studenti_blueprint.route("/<string:godina_upisa>/<string:broj_indeksa>", methods=["DELETE"])
def ukloni_jednog_studenta(godina_upisa, broj_indeksa):
    db = mysql.get_db()
    cursor = db.cursor()
    modified = cursor.execute("DELETE FROM studenti.studenti WHERE brojIndeksa = %s", (godina_upisa + "/" + broj_indeksa,))
    db.commit()
    
    if modified == 0:
        return flask.jsonify(None), 404
    
    return flask.jsonify(None), 200