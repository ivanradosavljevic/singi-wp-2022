import flask
from flask.blueprints import Blueprint

from utils.db import mysql
from utils.jwt import generate_token

login_blueprint = Blueprint("login", __name__)

@login_blueprint.route("/", methods=["POST"])
def login():
    if(flask.request.json["username"] == "korisnik" and flask.request.json["password"] == "lozinka"):
        token = generate_token(flask.current_app.config["SECRET"], {"sub": "korisnik", "uloge": ["ADMIN", "MANAGER"]})
        return flask.jsonify({
            "token": token
        })
    return flask.jsonify(""), 403