import flask
from flask import Flask

app = Flask(__name__, static_url_path="/", static_folder="static")

studenti = [
    {"brojIndeksa": "1234/123456", "ime": "Petar", "prezime": "Marković", "prosecnaOcena": 7},
    {"brojIndeksa": "1234/123457", "ime": "Marko", "prezime": "Petrović", "prosecnaOcena": 10},
    {"brojIndeksa": "1234/123458", "ime": "Jelena", "prezime": "Simić", "prosecnaOcena": 9},
    {"brojIndeksa": "1234/123459", "ime": "Ana", "prezime": "Petrović", "prosecnaOcena": 6},
]

@app.route("/")
def home():
    redovi_tabele = []
    for s in studenti:
        redovi_tabele.append('''
            <tr>
                <td>{brojIndeksa}</td>
                <td>{ime}</td>
                <td>{prezime}</td>
                <td>{prosecnaOcena}</td>
            </tr>
        '''.format(**s))
    
    stranica = '''
<!DOCTYPE html>
<html>

<head>
    <title>Naslov</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <a href="forma.html">Dodaj studenta</a>
    <table>
        <thead>
            <tr>
                <th>Broj indeksa</th>
                <th>Ime</th>
                <th>Prezime</th>
                <th>Prosečna ocena</th>
            </tr>
        </thead>

        <tbody>
        {}
        </tbody>

        <tfoot>
        </tfoot>
    </table>
</body>

</html>
    '''.format("\n".join(redovi_tabele))
    return stranica

@app.route("/dodajStudenta", methods=["GET", "POST"])
def dodaj_studenta():
    if flask.request.method == "POST":
        student = dict(flask.request.form)
    elif flask.request.method == "GET":
        student = dict(flask.request.args)

    studenti.append(student)
    print(student)
    return flask.redirect("/")

if __name__  == "__main__":
    app.run()