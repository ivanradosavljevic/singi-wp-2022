import flask
from flask import Flask

app = Flask(__name__, static_url_path="/", static_folder="static")

smerovi = [
    {"sifraSmera": "SII", "naziv": "Softversko i informaciono inzenjerstvo"},
    {"sifraSmera": "IT", "naziv": "Informacione tehnologije"},
]

studenti = [
    {"brojIndeksa": "1234/123456", "ime": "Petar", "prezime": "Marković", "prosecnaOcena": 7, "smer": "SII", "obrisan": False},
    {"brojIndeksa": "1234/123457", "ime": "Marko", "prezime": "Petrović", "prosecnaOcena": 10, "obrisan": False, "smer": "SII"},
    {"brojIndeksa": "1234/123458", "ime": "Jelena", "prezime": "Simić", "prosecnaOcena": 9, "obrisan": False, "smer": "SII"},
    {"brojIndeksa": "1234/123459", "ime": "Ana", "prezime": "Petrović", "prosecnaOcena": 6, "obrisan": False, "smer": "SII"},
]

@app.route("/")
def home():
    if flask.request.args.get("za_brisanje"):
        studenti[int(flask.request.args.get("za_brisanje"))]["obrisan"] = True
    for s in studenti:
        for smer in smerovi:
            if smer["sifraSmera"] == s.get("smer"):
                s["smer"] = smer
                break
    return flask.render_template("index.tpl.html", studenti=studenti)

@app.route("/forma.html")
def forma_za_dodavanje():
    return flask.render_template("formaZaIzmenu.tpl.html", indeks=None, smerovi=smerovi, student=None)

@app.route("/formaZaIzmenu")
def stranica_za_izmenu():
    student = studenti[int(flask.request.args["za_izmenu"])]
    return flask.render_template("formaZaIzmenu.tpl.html", indeks=flask.request.args["za_izmenu"], student=student, smerovi=smerovi)

@app.route("/izmeniStudenta", methods=["POST"])
def izmeni_studenta():
    studenti[int(flask.request.form["za_izmenu"])] = dict(flask.request.form)
    print(studenti)
    return flask.redirect("/")

@app.route("/dodajStudenta", methods=["GET", "POST"])
def dodaj_studenta():
    if flask.request.method == "POST":
        student = dict(flask.request.form)
    elif flask.request.method == "GET":
        student = dict(flask.request.args)

    studenti.append(student)
    print(student)
    return flask.redirect("/")

if __name__  == "__main__":
    app.run()