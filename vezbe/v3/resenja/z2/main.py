# Roba:
# Id, naziv, proizvodjac, cena

import flask
from flask import Flask

roba = [{"id": 1, "naziv": "Proizvod 1", "proizvodjac": "Proizvodjac", "cena": 100}]

app = Flask(__name__, static_url_path="")


@app.route("/")
def home():
    return flask.render_template("index.tpl.html", roba=roba)

@app.route("/dodajRobu", methods=["POST"])
def dodaj_robu():
    roba.append(flask.request.form)
    return flask.redirect("/")