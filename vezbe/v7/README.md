# Vežbe 7
*Cilj vežbi: Upotreba sesije.*

## Zadaci
1. Aplikaciju realizovanu na vežbama 4 prepraviti tako da se uvede koncept korisnika. Korisnik je opisan korisničkim imenom, lozinkom i tipom. Tip korisnika može biti administrator i profesor.
2. Omogućiti prijavu korisnika na sistem.
3. Onemogućiti prikaz podataka o studentima i smerovima svim neprijavljenim korisnicima. Neprijavljene korisnike automatski preusmeriti na stranicu za prijavu.
4. Administratorima omogućiti sve CRUD operacije nad studentima i smerovima. Profesorima samo omogućiti pregled podataka dostupnih u sistemu.
___
### Dodatne napomene:
* Dokumentacija za Flask radni okvir: https://palletsprojects.com/p/flask/.
* Dokumentacija za Jinja2 šablone: https://palletsprojects.com/p/jinja/
___
