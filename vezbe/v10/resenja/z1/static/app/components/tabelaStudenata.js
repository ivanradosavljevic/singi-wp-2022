export default {
    props: ["studenti"],
    methods: {
        ukloni(indeks) {
            axios.delete(`api/studenti/${indeks}`).then(response => {
                axios.get("api/studenti").then(response => {
                    this.studenti = response.data;
                });
            });
        },
    },
    template: `
    <table>
    <thead>
        <tr>
            <th>Broj indeksa</th>
            <th>Ime</th>
            <th>Prezime</th>
            <th>Prosečna ocena</th>
            <th>Smer</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        <tr v-for="(s, indeks) in studenti">
            <td>{{s.brojIndeksa}}</td>
            <td>{{s['ime']}}</td>
            <td>{{s.prezime}}</td>
            <td>{{s.prosecnaOcena}}</td>
            <td>{{s.naziv}} [{{s.sifraSmera}}]</td>
            <td>
                <button @click="ukloni(s.brojIndeksa)">Ukloni</button>
                <button @click="odaberiStudenta(s, indeks)">Izmeni</button>
            </td>
        </tr>
    </tbody>
    <tfoot>
    </tfoot>
</table>
    `
}