export default {
    emits: ["kreiranjeStudenta"],
    created() {
        axios.get("api/smerovi").then(response => {
            this.smerovi = response.data;
        });
    },
    data() {
        return {
            noviStudent: {},
            smerovi: []
        }
    },
    methods: {
        dodajStudenta() {
            this.$emit("kreiranjeStudenta", { ...this.noviStudent });
            // console.log(this.noviStudent);
        }
    },
    template: `
    <form v-on:submit.prevent="dodajStudenta">
        <div>
            <label for="brojIndeksa">Broj indeksa: </label>
            <input v-model="noviStudent.brojIndeksa" name="brojIndeksa" required>
        </div>
        <div>
            <label>Smer: <select v-model="noviStudent.smer" name="smer">
                    <option v-for="smer in smerovi" :value="smer">{{smer.naziv}}</option>
                </select>
            </label>
        </div>
        <div>
            <label>Ime: <input v-model="noviStudent.ime" name="ime" required></label>
        </div>
        <div>
            <label>Prezime: <input v-model="noviStudent.prezime" name="prezime" required></label>
        </div>
        <div>
            <label>Prosečna ocena: <input v-model="noviStudent.prosecnaOcena" type="number" name="prosecnaOcena"
                    min="5" max="10" required></label>
        </div>
        <div>
            <button type="submit">Dodaj</button>
        </div>
    </form>
    `
}