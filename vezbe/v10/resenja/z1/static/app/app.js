import TabelaStudenata from './components/tabelaStudenata.js';
import FormaStudent from './components/formaStudent.js';

const app = Vue.createApp({
    data() {
        return {studenti: []}
    },
    created() {
        axios.get("api/studenti").then(response => {
            this.studenti = response.data;
        });
    },
    methods: {
        provera(event) {
            console.log(event);
        },
        dodajStudenta(event) {
            console.log(event)
            let student = { ...event };
            student["smer"] = student["smer"]["id"];
            axios.post("api/studenti", student).then(response => {
                axios.get("api/studenti").then(response => {
                    this.studenti = response.data;
                });
            });
        },
        izmeniStudenta(event) {
            let student = { ...this.studentZaIzmenu };
            axios.put(`api/studenti/${student['brojIndeksa']}`, student).then(response => {
                axios.get("api/studenti").then(response => {
                    this.studenti = response.data;
                });
            });
        },
        odaberiStudenta(student, indeks) {
            this.studentZaIzmenu = { ...student };
            this.studentZaIzmenu["indeks"] = indeks
            this.studentZaIzmenu["smer"] = this.studentZaIzmenu["smer_id"];
        }
    }
})

app.component("tabela-studenata", TabelaStudenata);
app.component("forma-student", FormaStudent);

app.mount('#app');