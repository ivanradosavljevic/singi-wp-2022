import flask
from flask import Flask
from flaskext.mysql import MySQL
import pymysql

app = Flask(__name__, static_url_path="/", static_folder="static")
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "studenti"

@app.route("/")
def home():
    db = mysql.get_db()
    cursor = db.cursor()
    if flask.request.args.get("za_brisanje"):
        cursor.execute("DELETE FROM studenti WHERE brojIndeksa=%s", (flask.request.args.get("za_brisanje"), ))
        db.commit()
    cursor.execute("SELECT * FROM studenti.studenti LEFT JOIN smerovi ON studenti.smer_id = smerovi.id")
    studenti = cursor.fetchall()
    for s in studenti:
        s["smer"] = {"id": s["id"], "sifraSmera": s["sifraSmera"], "naziv": s["naziv"]}

    return flask.render_template("index.tpl.html", studenti=studenti)

@app.route("/forma.html")
def forma_za_dodavanje():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM smerovi")
    smerovi = cursor.fetchall()

    return flask.render_template("formaZaIzmenu.tpl.html", indeks=None, smerovi=smerovi, student=None)

@app.route("/formaZaIzmenu")
def stranica_za_izmenu():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM smerovi")
    smerovi = cursor.fetchall()
    cursor.execute("SELECT * FROM studenti WHERE brojIndeksa=%(za_izmenu)s", flask.request.args)
    student = cursor.fetchone()
    return flask.render_template("formaZaIzmenu.tpl.html", indeks=flask.request.args["za_izmenu"], student=student, smerovi=smerovi)

@app.route("/izmeniStudenta", methods=["POST"])
def izmeni_studenta():
    db = mysql.get_db()
    cursor = db.cursor()

    cursor.execute("UPDATE studenti SET brojIndeksa=%(brojIndeksa)s, ime=%(ime)s, prezime=%(prezime)s, prosecnaOcena=%(prosecnaOcena)s, smer_id=%(smer)s WHERE brojIndeksa=%(za_izmenu)s", flask.request.form)
    db.commit()
    return flask.redirect("/")

@app.route("/dodajStudenta", methods=["GET", "POST"])
def dodaj_studenta():
    db = mysql.get_db()
    cursor = db.cursor()
    if flask.request.method == "POST":
        cursor.execute("INSERT INTO studenti (brojIndeksa, ime, prezime, prosecnaOcena, smer_id) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(prosecnaOcena)s, %(smer)s)", flask.request.form)
    elif flask.request.method == "GET":
        cursor.execute("INSERT INTO studenti (brojIndeksa, ime, prezime, prosecnaOcena, smer_id) VALUES(%(brojIndeksa)s, %(ime)s, %(prezime)s, %(prosecnaOcena)s, %(smer)s)", flask.request.args)
    db.commit()    
    return flask.redirect("/")

if __name__  == "__main__":
    app.run()