# Vežbe 4
*Cilj vežbi: Skladištenje i dobavljanje podataka iz baze podataka.*

## Zadaci
1. Na osnovu primera sa prethodnih vežbi napraviti model šeme baze podataka za skladištenje podataka o studentima.
2. Instalirati Flask-MySQL biblioteku: ```pip install flask-mysql```
3. Uvezati bazu podataka sa postojećom Flask aplikacijom.
4. Prepraviti aplikaciju tako da se omogući dodavanje, uklanjanje, izmena i pregledanje podataka uskladištenih u bazi podataka.
___
### Dodatne napomene:
* Dokumentacija za Flask radni okvir: https://palletsprojects.com/p/flask/.
* Dokumentacija za Jinja radni okvir: https://palletsprojects.com/p/jinja/.
* Dokumentacija za Flask-MySQL biblioteku: https://flask-mysql.readthedocs.io/en/stable/.
___