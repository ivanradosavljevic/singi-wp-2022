import flask
from flask import Flask
from flaskext.mysql import MySQL
import pymysql

from contextlib import closing


app = Flask(__name__, static_url_path="/")
mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor)

app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = "root"
app.config["MYSQL_DATABASE_DB"] = "prodavnica"


@app.route("/")
def home():
    return app.send_static_file("index.html")

@app.route("/kupci", methods=["GET"])
def kupci_tabela():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac")
    kupci = cursor.fetchall()
    return flask.render_template("kupci.tpl.html", kupci=kupci)

@app.route("/kupci", methods=["POST"])
def dodavanje_kupca():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("INSERT INTO kupac(korisnickoIme, lozinka, ime, prezime) VALUES(%(korisnickoIme)s, %(ime)s, %(prezime)s, %(lozinka)s)", flask.request.form)
    db.commit()
    return flask.redirect("/kupci")

@app.route("/kupci/<int:id>/formaZaIzmenu", methods=["GET"])
def kupac_forma_izmena(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac WHERE id = %s", (id, ))
    kupac = cursor.fetchone()
    return flask.render_template("kupacIzmena.tpl.html", kupac=kupac)

@app.route("/kupci/<int:id>/detalji", methods=["GET"])
def kupac_detalji(id):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM kupac WHERE id = %s", (id, ))
    kupac = cursor.fetchone()
    return flask.render_template("kupac.tpl.html", kupac=kupac)

@app.route("/kupci/<int:id>/obrisi", methods=["GET"])
def brisanje_kupca(id):
    db = mysql.get_db()

    with closing(db.cursor()) as cursor:
        cursor.execute("DELETE FROM kupac WHERE id = %s", (id, ))
        db.commit()
    return flask.redirect("/kupci")


@app.route("/kupci/<int:id>/izmena", methods=["POST"])
def izmena_kupca(id):
    db = mysql.get_db()
    with closing(db.cursor()) as cursor:
        kupac = dict(flask.request.form)
        kupac["id"] = id
        print(kupac)
        if kupac.get("lozinka", "") != "":
            cursor.execute("UPDATE kupac SET korisnickoIme=%(korisnickoIme)s, ime=%(ime)s, prezime=%(prezime)s, lozinka=%(lozinka)s WHERE id = %(id)s", kupac)
        else:
            cursor.execute("UPDATE kupac SET korisnickoIme=%(korisnickoIme)s, ime=%(ime)s, prezime=%(prezime)s WHERE id = %(id)s", kupac)
        db.commit()

    return flask.redirect("/kupci")

if __name__ == "__main__":
    app.run()